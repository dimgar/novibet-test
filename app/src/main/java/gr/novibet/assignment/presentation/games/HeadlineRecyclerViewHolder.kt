package gr.novibet.assignment.presentation.games

import androidx.recyclerview.widget.RecyclerView
import gr.novibet.assignment.databinding.ItemHeadlineRecyclerBinding
import gr.novibet.assignment.presentation.headlines.HeadlineViewModel
import gr.novibet.assignment.presentation.headlines.HeadlinesAdapter

class HeadlineRecyclerViewHolder(
    private val itemBinding: ItemHeadlineRecyclerBinding,
    private val headlinesAdapter: HeadlinesAdapter
) : RecyclerView.ViewHolder(itemBinding.root) {
    fun bind(items: List<HeadlineViewModel>) {
        if (itemBinding.itemHeadlineRecyclerRecyclerView.adapter == null) {
            itemBinding.itemHeadlineRecyclerRecyclerView.adapter = headlinesAdapter
        }

//        headlinesAdapter.setItems(items.toMutableList(), itemBinding.itemHeadlineRecyclerRecyclerView)
    }
}