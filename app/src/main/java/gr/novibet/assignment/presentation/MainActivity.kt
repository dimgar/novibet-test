package gr.novibet.assignment.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import gr.novibet.assignment.R
import gr.novibet.assignment.data.State
import gr.novibet.assignment.data.scheduler.SchedulerProvider
import gr.novibet.assignment.databinding.ActivityMainBinding
import gr.novibet.assignment.presentation.games.GameViewModel
import gr.novibet.assignment.presentation.headlines.HeadlineViewModel
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasAndroidInjector {
    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var scheduleProviders: SchedulerProvider

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(MainActivityViewModel::class.java)
    }

    private var binding: ActivityMainBinding? = null

    private val gamesAdapter by lazy { VerticalAdapter() }

    private val gamesObserver by lazy {
        Observer<State<List<GameViewModel>>> {
            when (it) {
                is State.Loading -> {

                }
                is State.Error -> {

                }
                is State.Success -> {
                    gamesAdapter.setItems(it.data.toMutableList())
                }
            }
        }
    }

    private val headlinesObserver by lazy {
        Observer<State<List<HeadlineViewModel>>> {
            when (it) {
                is State.Loading -> {

                }
                is State.Error -> {

                }
                is State.Success -> {
                    gamesAdapter.setHeadlines(it.data.toMutableList())
                }
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)

        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        binding?.activityMainRecyclerView?.adapter = gamesAdapter
        gamesAdapter.setItems(mutableListOf())
        binding?.viewModel = viewModel

        viewModel.games.observe(this, gamesObserver)
        viewModel.headlines.observe(this, headlinesObserver)
//
        viewModel.getGames()
        viewModel.getHeadlines()
    }


    override fun androidInjector(): AndroidInjector<Any?>? {
        return androidInjector
    }
}