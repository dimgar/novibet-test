package gr.novibet.assignment.presentation.base.binder

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso
import gr.novibet.assignment.R

@BindingAdapter("networkSrc")
fun ImageView.networkSrc(url: String?) {

    val picasso = Picasso.get()

    if (url.isNullOrBlank()) {
        picasso
            .load(R.drawable.ic_launcher_background)
            .error(R.drawable.ic_launcher_background)
            .into(this)
        return
    }

    picasso
        .load(url)
        .placeholder(R.drawable.ic_launcher_background)
        .error(R.drawable.ic_launcher_background)
        .into(this)
}

