package gr.novibet.assignment.presentation.base.toast

import androidx.annotation.StringRes

interface MToast {
    fun show(@StringRes message: Int?)
    fun show(message: String?)
}
