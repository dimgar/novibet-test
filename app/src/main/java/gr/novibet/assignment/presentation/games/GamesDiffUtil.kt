package gr.novibet.assignment.presentation.games

import androidx.recyclerview.widget.DiffUtil
import gr.novibet.assignment.presentation.BaseViewModel

class GamesDiffUtil(
    private val newList: List<BaseViewModel>,
    private val oldList: List<BaseViewModel>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]

        return oldItem.viewType == newItem.viewType && oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]

        return oldItem == newItem
    }
}