package gr.novibet.assignment.presentation.headlines

import androidx.recyclerview.widget.RecyclerView
import gr.novibet.assignment.databinding.ItemHeadlineBinding

class HeadlineViewHolder(
    private val itemBinding: ItemHeadlineBinding
): RecyclerView.ViewHolder(itemBinding.root) {
   fun bind(viewModel: HeadlineViewModel) {
        itemBinding.viewModel = viewModel
        itemBinding.executePendingBindings()
    }
}