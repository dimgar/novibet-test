package gr.novibet.assignment.presentation.headlines

import androidx.databinding.BaseObservable

data class HeadlineViewModel(
    val id: String?,
    val competitor1: String?,
    val competitor2: String?,
    val startTime: String?
//    private val gameFinishedMessage: String
): BaseObservable()