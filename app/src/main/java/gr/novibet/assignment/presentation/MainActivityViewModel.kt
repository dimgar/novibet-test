package gr.novibet.assignment.presentation

import android.view.View
import androidx.databinding.Bindable
import androidx.lifecycle.MutableLiveData
import gr.novibet.assignment.BR
import gr.novibet.assignment.data.State
import gr.novibet.assignment.domain.games.GamesUseCase
import gr.novibet.assignment.domain.headlines.HeadlinesUseCase
import gr.novibet.assignment.presentation.base.ObservableViewModel
import gr.novibet.assignment.presentation.base.toast.MToast
import gr.novibet.assignment.presentation.games.GameViewModel
import gr.novibet.assignment.presentation.headlines.HeadlineViewModel
import io.reactivex.Flowable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MainActivityViewModel @Inject constructor(
    private val gamesUseCase: GamesUseCase,
    private val headlinesUseCase: HeadlinesUseCase,
    private val toast: MToast
) : ObservableViewModel() {

    private val compositeDisposable = CompositeDisposable()

    val games = MutableLiveData<State<List<GameViewModel>>>()
    val headlines = MutableLiveData<State<List<HeadlineViewModel>>>()

    var progressBarVisibility = View.VISIBLE
        set(value) {
            field = value
            notifyPropertyChanged(BR.progressBarVisibility)
        }
        @Bindable get

    fun getGames() {
        gamesUseCase.games().toFlowable()
            .flatMap {
                games.postValue(it)
                progressBarVisibility = View.GONE

                Flowable.timer(2, TimeUnit.SECONDS)
            }
            .flatMap {
                gamesUseCase.updatedGames()
            }
            .subscribeBy(onNext = {
                games.postValue(it)

            }, onError = {
                onError(it)
            })
            .addTo(compositeDisposable)
    }

    fun getHeadlines() {
        headlinesUseCase.headlines().toFlowable()
            .flatMap {
                headlines.postValue(it)
                progressBarVisibility = View.GONE

                Flowable.timer(2, TimeUnit.SECONDS)

            }
            .flatMap {
                headlinesUseCase.updatedHeadlines()
            }
            .subscribeBy(onNext = {
                headlines.postValue(it)
            }, onError = {
                onError(it)
            })
            .addTo(compositeDisposable)

    }

    private fun onError(t: Throwable) {
        Timber.e(t)
        toast.show(t.message)
        progressBarVisibility = View.GONE
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}