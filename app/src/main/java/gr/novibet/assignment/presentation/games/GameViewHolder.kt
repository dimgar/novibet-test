package gr.novibet.assignment.presentation.games

import androidx.recyclerview.widget.RecyclerView
import gr.novibet.assignment.databinding.ItemGameBinding

class GameViewHolder(
    private val itemBinding: ItemGameBinding
): RecyclerView.ViewHolder(itemBinding.root) {

    fun bind(viewModel: GameViewModel) {
        itemBinding.viewModel?.clear()

        itemBinding.viewModel = viewModel
        itemBinding.executePendingBindings()
    }
}