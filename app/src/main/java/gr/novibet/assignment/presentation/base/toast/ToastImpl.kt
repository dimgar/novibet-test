package gr.novibet.assignment.presentation.base.toast

import android.content.Context
import android.widget.Toast
import androidx.annotation.StringRes
import javax.inject.Inject

class ToastImpl @Inject constructor(private val applicationContext: Context): MToast {
    override fun show(@StringRes message: Int?) {
        message ?: return

        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
    }

    override fun show(message: String?) {
        message ?: return

        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
    }
}