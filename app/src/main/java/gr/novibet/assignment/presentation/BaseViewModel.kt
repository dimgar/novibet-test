package gr.novibet.assignment.presentation

import androidx.annotation.IntDef

@Retention(AnnotationRetention.SOURCE)
@IntDef(
    AdapterViewType.HEADLINE_LIST,
    AdapterViewType.GAME
)

annotation class AdapterViewType {
    companion object {
        const val HEADLINE_LIST = 0
        const val GAME = 1
    }
}

interface BaseViewModel {
    val id: String?
    @AdapterViewType val viewType: Int
}