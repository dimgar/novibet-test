package gr.novibet.assignment.presentation.base.getstring

import android.content.Context
import javax.inject.Inject

class GetStringImpl @Inject constructor(private val applicationContext: Context): GetString {
    override fun getString(stringRes: Int): String {
        return applicationContext.getString(stringRes)
    }

}