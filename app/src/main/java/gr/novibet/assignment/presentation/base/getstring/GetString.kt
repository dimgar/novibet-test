package gr.novibet.assignment.presentation.base.getstring

import androidx.annotation.StringRes

interface GetString {
    fun getString(@StringRes stringRes: Int): String
}