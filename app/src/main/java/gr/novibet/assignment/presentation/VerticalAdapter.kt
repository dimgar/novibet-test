package gr.novibet.assignment.presentation

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import gr.novibet.assignment.databinding.ItemGameBinding
import gr.novibet.assignment.databinding.ItemHeadlineRecyclerBinding
import gr.novibet.assignment.presentation.games.*
import gr.novibet.assignment.presentation.headlines.HeadlineViewModel
import gr.novibet.assignment.presentation.headlines.HeadlinesAdapter
import java.lang.ref.WeakReference

class VerticalAdapter() :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var items = mutableListOf<BaseViewModel>()
    private var headlines = mutableListOf<HeadlineViewModel>()

    private val headlinesAdapter = HeadlinesAdapter()

    // Caching vars
    private var layoutInflaterWeakReference: WeakReference<LayoutInflater>? = null
    //

    fun setItems(newItems: MutableList<BaseViewModel>) {
        if (this.items.size == 0) {
            newItems.add(0,
                HeadlineRecyclerViewModel()
            )
        } else {
            newItems.add(0, this.items[0])
        }

        val diffResult = DiffUtil.calculateDiff(
            GamesDiffUtil(
                newItems,
                this.items
            )
        )

        this.items = newItems
        diffResult.dispatchUpdatesTo(this)
    }

    fun setHeadlines(headlines: MutableList<HeadlineViewModel>) {
        headlinesAdapter.setItems(headlines)
    }

    override fun getItemViewType(position: Int): Int {
        return items[position].viewType
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (layoutInflaterWeakReference?.get() == null) {
            layoutInflaterWeakReference = WeakReference(LayoutInflater.from(parent.context))
        }

        when (viewType) {
            AdapterViewType.GAME -> {
                layoutInflaterWeakReference?.get()?.run {
                    return GameViewHolder(
                        ItemGameBinding.inflate(this, parent, false)
                    )
                }
            }
            AdapterViewType.HEADLINE_LIST -> {
                layoutInflaterWeakReference?.get()?.run {
                    return HeadlineRecyclerViewHolder(
                        ItemHeadlineRecyclerBinding.inflate(
                            this,
                            parent,
                            false
                        ), headlinesAdapter
                    )
                }
            }
        }

        return GameViewHolder(
            ItemGameBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is GameViewHolder -> {
                holder.bind(items[position] as GameViewModel)
            }
            is HeadlineRecyclerViewHolder -> {
                holder.bind(headlines)
            }
        }

    }

    fun clearGameViewModelElapsedTimeJobs() {
        items.forEach {
            when (it) {
                is GameViewModel -> {
                    it.clear()
                }
            }
        }
    }

}