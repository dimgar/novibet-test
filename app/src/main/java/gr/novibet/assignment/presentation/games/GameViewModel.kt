package gr.novibet.assignment.presentation.games

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import gr.novibet.assignment.BR
import gr.novibet.assignment.data.scheduler.SchedulerProvider
import gr.novibet.assignment.domain.games.toMinutesSeconds
import gr.novibet.assignment.presentation.AdapterViewType
import gr.novibet.assignment.presentation.BaseViewModel
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.coroutines.*
import org.threeten.bp.LocalTime
import timber.log.Timber
import java.util.concurrent.TimeUnit
import kotlin.coroutines.CoroutineContext

class GameViewModel(
    override val id: String?,
    val competitor1: String?,
    val competitor2: String?,
    elapsedLocalTimeInitial: LocalTime?,
    private val schedulerProvider: SchedulerProvider,
    gameFinishedMessage: String
): BaseObservable(), BaseViewModel, CoroutineScope {

    @AdapterViewType
    override val viewType = AdapterViewType.GAME

    var elapsedLocalTime = elapsedLocalTimeInitial

    override val coroutineContext: CoroutineContext = Dispatchers.Main

//    var compositeDisposable: CompositeDisposable? = null

    var elapsedLayoutValue = elapsedLocalTimeInitial?.toMinutesSeconds() ?: gameFinishedMessage
        set(value) {
            field = value
            notifyPropertyChanged(BR.elapsedLayoutValue)
        }
        @Bindable get

    private var elapsedTimeJob: Job? = null

    fun repeatFun(): Job {
        return launch {
            while(isActive) {
                delay(1000)
                elapsedLocalTime = elapsedLocalTime?.plusSeconds(1)
                elapsedLayoutValue = elapsedLocalTime?.toMinutesSeconds() ?: elapsedLayoutValue
            }
        }
    }

    init {
        elapsedTimeJob = repeatFun()
    }

    // Or with Rx instead of coroutines
    // initElapsedTime().addTo(compositeDisposable)
    private fun initElapsedTime(): Disposable {
        return Observable
            .interval(1, TimeUnit.SECONDS)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribeBy(onNext = {
                elapsedLocalTime = elapsedLocalTime?.plusSeconds(1)
                elapsedLayoutValue = elapsedLocalTime?.toMinutesSeconds() ?: elapsedLayoutValue
            }, onError = {
                Timber.e(it)
            })
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GameViewModel

        if (id != other.id) return false
        if (competitor1 != other.competitor1) return false
        if (competitor2 != other.competitor2) return false
        if (elapsedLocalTime != other.elapsedLocalTime) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + competitor1.hashCode()
        result = 31 * result + competitor2.hashCode()
        result = 31 * result + elapsedLocalTime.hashCode()
        return result
    }

    fun clear() {
        elapsedTimeJob?.cancel()
    }
}