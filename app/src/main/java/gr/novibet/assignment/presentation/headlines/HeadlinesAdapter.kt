package gr.novibet.assignment.presentation.headlines

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import gr.novibet.assignment.databinding.ItemHeadlineBinding
import java.lang.ref.WeakReference

class HeadlinesAdapter(): RecyclerView.Adapter<HeadlineViewHolder>() {
    private var items = mutableListOf<HeadlineViewModel>()

    // Caching vars
    private var layoutInflaterWeakReference: WeakReference<LayoutInflater>? = null
    //

    fun setItems(newItems: MutableList<HeadlineViewModel>) {
        val diffResult = DiffUtil.calculateDiff(
            HeadlineDiffUtil(
                newItems,
                this.items
            )
        )
        this.items = newItems
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeadlineViewHolder {
        if (layoutInflaterWeakReference?.get() == null) {
            layoutInflaterWeakReference = WeakReference(LayoutInflater.from(parent.context))
        }

        layoutInflaterWeakReference?.get()?.run {
            return HeadlineViewHolder(ItemHeadlineBinding.inflate(this, parent, false))
        }

        return HeadlineViewHolder(ItemHeadlineBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: HeadlineViewHolder, position: Int) {
        holder.bind(items[position])
    }

}