package gr.novibet.assignment.presentation.games

import androidx.databinding.BaseObservable
import gr.novibet.assignment.presentation.AdapterViewType
import gr.novibet.assignment.presentation.BaseViewModel

class HeadlineRecyclerViewModel: BaseObservable(), BaseViewModel {
    override val id: String = "-500"

    @AdapterViewType
    override val viewType = AdapterViewType.HEADLINE_LIST

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as HeadlineRecyclerViewModel

        if (id != other.id) return false
        if (viewType != other.viewType) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + viewType
        return result
    }


}