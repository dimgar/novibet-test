package gr.novibet.assignment.di.module

import androidx.lifecycle.ViewModel
import gr.novibet.assignment.di.lib.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import gr.novibet.assignment.presentation.MainActivityViewModel

@Module
interface MainActivityModule {
    @Binds
    @IntoMap
    @ViewModelKey(MainActivityViewModel::class)
    fun bindViewModel(productListViewModel: MainActivityViewModel): ViewModel
}