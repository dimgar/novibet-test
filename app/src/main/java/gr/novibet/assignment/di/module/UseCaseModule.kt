package gr.novibet.assignment.di.module

import dagger.Module
import dagger.Provides
import gr.novibet.assignment.data.api.NetworkSource
import gr.novibet.assignment.data.scheduler.SchedulerProvider
import gr.novibet.assignment.data.scheduler.SchedulerProviderImpl
import gr.novibet.assignment.domain.headlines.HeadlinesUseCase
import gr.novibet.assignment.domain.headlines.HeadlinesUseCaseImpl
import gr.novibet.assignment.domain.games.GamesUseCase
import gr.novibet.assignment.domain.games.GamesUseCaseImpl
import gr.novibet.assignment.presentation.base.getstring.GetString
import javax.inject.Singleton

@Module
object UseCaseModule {

    @Singleton
    @Provides
    @JvmStatic
    fun provideSchedulerProvider(): SchedulerProvider {
        return SchedulerProviderImpl()
    }

    @Singleton
    @Provides
    @JvmStatic
    fun providesGamesUseCase(
        networkSource: NetworkSource,
        schedulerProvider: SchedulerProvider,
        getString: GetString
    ): GamesUseCase {
        return GamesUseCaseImpl(
            networkSource,
            schedulerProvider,
            getString
        )
    }

    @Singleton
    @Provides
    @JvmStatic
    fun providesHeadlinesUseCase(
        networkSource: NetworkSource,
        schedulerProvider: SchedulerProvider): HeadlinesUseCase {
        return HeadlinesUseCaseImpl(
            networkSource,
            schedulerProvider
        )
    }
}