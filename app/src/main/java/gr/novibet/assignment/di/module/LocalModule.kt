package gr.novibet.assignment.di.module

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import gr.novibet.assignment.data.local.LocalSource
import gr.novibet.assignment.data.local.LocalSourceImpl
import javax.inject.Singleton

@Module
object LocalModule {
    @Provides
    @Singleton
    @JvmStatic
    internal fun providesContext(application: Application): Context {
        return application
    }
    @Provides
    @Singleton
    @JvmStatic
    fun providesLocalSource(): LocalSource {
        return LocalSourceImpl()
    }
}