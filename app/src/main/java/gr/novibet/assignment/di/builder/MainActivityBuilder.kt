package gr.novibet.assignment.di.builder

import gr.novibet.assignment.di.module.MainActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector
import gr.novibet.assignment.presentation.MainActivity

@Module
interface MainActivityBuilder {
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    fun contributeActivity(): MainActivity
}