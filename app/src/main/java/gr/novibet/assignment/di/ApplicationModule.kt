package gr.novibet.assignment.di

import android.app.Application
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import gr.novibet.assignment.presentation.base.getstring.GetString
import gr.novibet.assignment.presentation.base.getstring.GetStringImpl
import gr.novibet.assignment.presentation.base.toast.MToast
import gr.novibet.assignment.presentation.base.toast.ToastImpl
import gr.novibet.assignment.data.api.model.response.games.LiveData
import gr.novibet.assignment.data.api.serializer.LiveDataSerializer
import javax.inject.Singleton

@Module
object ApplicationModule {

    @Provides
    @Singleton
    @JvmStatic
    fun providesGson(): Gson {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.registerTypeAdapter(LiveData::class.java, LiveDataSerializer())

        return gsonBuilder.create()
    }

    @Provides
    @Singleton
    @JvmStatic
    fun providesToast(application: Application): MToast =
        ToastImpl(application)

    @Provides
    @Singleton
    @JvmStatic
    fun providesGetString(application: Application): GetString =
        GetStringImpl(application)
}