package gr.novibet.assignment.di

import android.app.Application
import gr.novibet.assignment.di.builder.MainActivityBuilder
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import gr.novibet.assignment.NovibetAssignmentApplication
import gr.novibet.assignment.di.module.LocalModule
import gr.novibet.assignment.di.module.NetworkModule
import gr.novibet.assignment.di.module.UseCaseModule
import gr.novibet.assignment.di.module.ViewModelModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    ApplicationModule::class,
    NetworkModule::class,
    ViewModelModule::class,
    UseCaseModule::class,
    LocalModule::class,
    MainActivityBuilder::class
])
interface ApplicationComponent : AndroidInjector<NovibetAssignmentApplication> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): ApplicationComponent
    }

    override fun inject(application: NovibetAssignmentApplication)
}


