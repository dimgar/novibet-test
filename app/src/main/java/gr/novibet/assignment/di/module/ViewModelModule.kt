package gr.novibet.assignment.di.module

import androidx.lifecycle.ViewModelProvider
import gr.novibet.assignment.di.lib.ViewModelFactory
import dagger.Binds
import dagger.Module



@Module
interface ViewModelModule {
    @Binds
    fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}