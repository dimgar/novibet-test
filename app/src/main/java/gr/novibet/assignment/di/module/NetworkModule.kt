package gr.novibet.assignment.di.module

import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import gr.novibet.assignment.BuildConfig
import gr.novibet.assignment.data.api.NetworkSource
import gr.novibet.assignment.domain.token.TokenInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
object NetworkModule {

    @Provides
    @Singleton
    @JvmStatic
    fun providesOkHtppClient(tokenInterceptor: TokenInterceptor): OkHttpClient {
        val httpClient = OkHttpClient.Builder()
            .readTimeout(0, TimeUnit.SECONDS)
            .connectTimeout(0, TimeUnit.SECONDS)

        if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            httpClient.addInterceptor(loggingInterceptor)
        }

        httpClient.addInterceptor (tokenInterceptor)

        return httpClient.build()
    }

    @Singleton
    @Provides
    @JvmStatic
    fun provideRetrofit(okhhttpClient: OkHttpClient, gson: Gson): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.API_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
            .client(okhhttpClient)
            .build()
    }

    @Singleton
    @Provides
    @JvmStatic
    fun provideNetworkSource(retrofit: Retrofit): NetworkSource {
        return retrofit.create(NetworkSource::class.java)
    }

}