package gr.novibet.assignment

import com.jakewharton.threetenabp.AndroidThreeTen
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import gr.novibet.assignment.di.DaggerApplicationComponent

class NovibetAssignmentApplication: DaggerApplication() {
    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this);
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerApplicationComponent.builder()
            .application(this)
            .build()
    }
}