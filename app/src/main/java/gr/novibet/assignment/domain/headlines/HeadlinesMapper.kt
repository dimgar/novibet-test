package gr.novibet.assignment.domain.headlines

import gr.novibet.assignment.data.api.model.response.headlines.HeadlinesResponse
import gr.novibet.assignment.presentation.headlines.HeadlineViewModel

fun List<HeadlinesResponse>.toHeadlineViewModels(): List<HeadlineViewModel> {

    return flatMap { it.betViews ?: listOf() }
        .filter { it.startTime != null }
        .map {
            HeadlineViewModel(
                it.betContextId,
                it.competitor1Caption,
                it.competitor2Caption,
                it.startTime
            )
        }
}