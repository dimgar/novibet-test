package gr.novibet.assignment.domain.token

import com.google.gson.Gson
import gr.novibet.assignment.BuildConfig
import gr.novibet.assignment.data.api.model.response.token.TokenResponse
import gr.novibet.assignment.data.local.LocalSource
import okhttp3.*
import javax.inject.Inject

class TokenInterceptor @Inject constructor(
    private val gson: Gson,
    private val localSource: LocalSource
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()

        if (localSource.authorizationHeader.isNullOrBlank()) {
            val body = MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("userName", "Dimitris")
                .addFormDataPart("password", "Garofalakis")
                .build()

            val refreshTokenRequest = Request.Builder()
                .url(BuildConfig.API_URL + BuildConfig.TOKEN_ENDPOINT)
                .post(body)
                .build()

            val refreshTokenResponse = chain.proceed(refreshTokenRequest)

            val refreshTokenBodyString = refreshTokenResponse.body()?.string()

            val convertedResponse = gson.fromJson(refreshTokenBodyString, TokenResponse::class.java)

            localSource.authorizationHeader = convertedResponse.authorizationHeader()
        }

        val formData = HashMap<String, String>()

        (originalRequest.body() as? FormBody?)?.run {
            for (i in 0 until size()) {
                formData[encodedName(i)] = encodedValue(i)
            }
        }

        formData["Authorization"] = localSource.authorizationHeader ?: ""

        val newRequestBody = MultipartBody.Builder().setType(MultipartBody.FORM)

        formData.forEach { (key, value) -> newRequestBody.addFormDataPart(key, value) }

        return chain.proceed(
            Request.Builder()
                .url(originalRequest.url())
                .post(newRequestBody.build())
                .build()
        )
    }

}