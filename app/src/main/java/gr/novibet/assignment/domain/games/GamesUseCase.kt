package gr.novibet.assignment.domain.games

import gr.novibet.assignment.data.State
import gr.novibet.assignment.presentation.games.GameViewModel
import io.reactivex.Flowable
import io.reactivex.Single

interface GamesUseCase {
    fun games(): Single<State<List<GameViewModel>>>
    fun updatedGames(): Flowable<State<List<GameViewModel>>>
}