package gr.novibet.assignment.domain.games

import gr.novibet.assignment.R
import gr.novibet.assignment.data.State
import gr.novibet.assignment.data.api.NetworkSource
import gr.novibet.assignment.data.scheduler.SchedulerProvider
import gr.novibet.assignment.presentation.base.getstring.GetString
import gr.novibet.assignment.presentation.games.GameViewModel
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class GamesUseCaseImpl @Inject constructor(
    private val networkSource: NetworkSource,
    private val schedulerProvider: SchedulerProvider,
    private val getString: GetString
) : GamesUseCase {
    override fun games(): Single<State<List<GameViewModel>>> {
        return networkSource.games()
            .map {
                State.success(
                    it.toGameViewModels(
                        schedulerProvider,
                        getString.getString(R.string.game_finished)
                    )
                )
            }
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())

    }

    override fun updatedGames(): Flowable<State<List<GameViewModel>>> {
        return Observable.interval(0, 2, TimeUnit.SECONDS)
            .flatMap {
                networkSource.updatedGames()
            }
            .map {
                State.success(
                    it.toGameViewModels(
                        schedulerProvider,
                        getString.getString(R.string.game_finished)
                    )
                )
            }
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .toFlowable(BackpressureStrategy.LATEST)
    }

}