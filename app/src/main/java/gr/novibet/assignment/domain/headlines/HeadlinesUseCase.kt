package gr.novibet.assignment.domain.headlines

import gr.novibet.assignment.data.State
import gr.novibet.assignment.presentation.headlines.HeadlineViewModel
import io.reactivex.Flowable
import io.reactivex.Single

interface HeadlinesUseCase {
    fun headlines(): Single<State<List<HeadlineViewModel>>>
    fun updatedHeadlines(): Flowable<State<List<HeadlineViewModel>>>
}