package gr.novibet.assignment.domain.headlines

import gr.novibet.assignment.data.State
import gr.novibet.assignment.data.api.NetworkSource
import gr.novibet.assignment.data.scheduler.SchedulerProvider
import gr.novibet.assignment.presentation.headlines.HeadlineViewModel
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class HeadlinesUseCaseImpl @Inject constructor(
    private val networkSource: NetworkSource,
    private val schedulerProvider: SchedulerProvider
//    private val getString: GetString
) : HeadlinesUseCase {
    override fun headlines(): Single<State<List<HeadlineViewModel>>> {
        return networkSource.headlines()
            .map {
                State.success(
                    it.toHeadlineViewModels()
                )
            }
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())

    }

    override fun updatedHeadlines(): Flowable<State<List<HeadlineViewModel>>> {
        return Observable.interval(0, 2, TimeUnit.SECONDS)
            .flatMap {
                networkSource.updatedHeadlines()
            }
            .map {
                State.success(
                    it.toHeadlineViewModels()
                )
            }
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .toFlowable(BackpressureStrategy.LATEST)
    }

}