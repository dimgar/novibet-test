package gr.novibet.assignment.domain.games

import gr.novibet.assignment.data.api.model.response.games.GamesResponse
import gr.novibet.assignment.data.scheduler.SchedulerProvider
import gr.novibet.assignment.presentation.games.GameViewModel
import org.threeten.bp.LocalTime
import org.threeten.bp.format.DateTimeFormatter

fun List<GamesResponse>.toGameViewModels(schedulerProvider: SchedulerProvider, gameFinishedMessage: String): List<GameViewModel> {

    return flatMap { it.betViews ?: listOf() }
        .flatMap { it.competitions ?: listOf() }
        .flatMap { it.events ?: listOf() }
        .map {
            GameViewModel(
                id = it.betContextId,
                competitor1 = it.additionalCaptions?.competitor1,
                competitor2 = it.additionalCaptions?.competitor2,
                elapsedLocalTimeInitial = it.liveData?.elapsedLocalTime,
                schedulerProvider = schedulerProvider,
                gameFinishedMessage = gameFinishedMessage
            )
        }
}

fun LocalTime.toMinutesSeconds(): String {
    return format(DateTimeFormatter.ofPattern("mm:ss"))
}