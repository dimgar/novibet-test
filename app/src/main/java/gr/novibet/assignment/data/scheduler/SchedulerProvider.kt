package gr.novibet.assignment.data.scheduler

import io.reactivex.Scheduler

interface SchedulerProvider {

    fun computation(): Scheduler

    fun newThread(): Scheduler

    fun io(): Scheduler

    fun ui(): Scheduler
}