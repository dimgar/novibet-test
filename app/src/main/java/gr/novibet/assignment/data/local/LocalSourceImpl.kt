package gr.novibet.assignment.data.local

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LocalSourceImpl @Inject constructor(): LocalSource {
    override var authorizationHeader: String? = null
}