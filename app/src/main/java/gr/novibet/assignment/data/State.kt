package gr.novibet.assignment.data

sealed class State<T> {

    class Loading<T> : State<T>()

    data class Error<T>(
        val errorMessage: String?,
        val error: Throwable?,
        val data: T? = null
    ) : State<T>()

    data class Success<T>(var data: T) : State<T>()

    companion object {
        fun <T> loading(data: T? = null): State<T> =
            Loading()
        fun <T> error(
            errorMessage: String,
            errorCode: Int? = null,
            error: Throwable? = null,
            data: T? = null
        ): State<T> =
            Error(
                errorMessage,
                error,
                data
            )

        fun <T> success(data: T): State<T> =
            Success(data)
    }
}