package gr.novibet.assignment.data.api.model.response.token

import com.google.gson.annotations.SerializedName

data class TokenResponse (
    @SerializedName("access_token")
    private val accessToken: String?,

    @SerializedName("token_type")
    private val tokenType: String?
) {
    fun authorizationHeader() = "$tokenType $accessToken"
}