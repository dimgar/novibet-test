package gr.novibet.assignment.data.local

interface LocalSource {
    var authorizationHeader: String?
}