package gr.novibet.assignment.data.api.model.response.games

import com.google.gson.annotations.SerializedName
import org.threeten.bp.LocalTime

/*
Copyright (c) 2020 Kotlin Data Classes Generated from JSON powered by http://www.json2kotlin.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */


data class LiveData (

	@SerializedName("homeGoals") val homeGoals : Int?,
	@SerializedName("awayGoals") val awayGoals : Int?,
	@SerializedName("homeCorners") val homeCorners : Int?,
	@SerializedName("awayCorners") val awayCorners : Int?,
	@SerializedName("homeYellowCards") val homeYellowCards : Int?,
	@SerializedName("awayYellowCards") val awayYellowCards : Int?,
	@SerializedName("homeRedCards") val homeRedCards : Int?,
	@SerializedName("awayRedCards") val awayRedCards : Int?,
	@SerializedName("homePenaltyKicks") val homePenaltyKicks : Int?,
	@SerializedName("awayPenaltyKicks") val awayPenaltyKicks : Int?,
	@SerializedName("supportsAchievements") val supportsAchievements : Boolean?,
	@SerializedName("liveStreamingCountries") val liveStreamingCountries : String?,
	@SerializedName("sportradarMatchId") val sportradarMatchId : String?,
	@SerializedName("referenceTime") val referenceTime : String?,
	@SerializedName("referenceTimeUnix") val referenceTimeUnix : Int?,
	@SerializedName("elapsed") val elapsed : String?,
	@SerializedName("elapsedSeconds") val elapsedSeconds : Double?,
	@SerializedName("duration") val duration : String?,
	@SerializedName("durationSeconds") val durationSeconds : String?,
	@SerializedName("timeToNextPhase") val timeToNextPhase : String?,
	@SerializedName("timeToNextPhaseSeconds") val timeToNextPhaseSeconds : String?,
	@SerializedName("phaseSysname") val phaseSysname : String?,
	@SerializedName("phaseCaption") val phaseCaption : String?,
	@SerializedName("phaseCaptionLong") val phaseCaptionLong : String?,
	@SerializedName("isLive") val isLive : Boolean?,
	@SerializedName("isInPlay") val isInPlay : Boolean?,
	@SerializedName("isInPlayPaused") val isInPlayPaused : Boolean?,
	@SerializedName("isInterrupted") val isInterrupted : Boolean?,
	@SerializedName("supportsActions") val supportsActions : Boolean?,
	@SerializedName("timeline") val timeline : String?,
	@SerializedName("adjustTimeMillis") val adjustTimeMillis : Int?
) {
	var elapsedLocalTime: LocalTime? = null
}