package gr.novibet.assignment.data.api.serializer

import com.google.gson.*
import gr.novibet.assignment.data.api.model.response.games.LiveData
import org.threeten.bp.LocalTime
import org.threeten.bp.format.DateTimeFormatter
import java.lang.reflect.Type


class LiveDataSerializer : JsonDeserializer<LiveData?> {
    @Throws(JsonParseException::class)
    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): LiveData? {

        val gson = Gson()
        val liveDataResponse = gson.fromJson(json, LiveData::class.java)

        val elapsedTime: LocalTime
        try  {
            elapsedTime = LocalTime.parse(liveDataResponse.elapsed, DateTimeFormatter.ofPattern("HH:mm:ss.SSSSSSS"))
        } catch (e: Exception) {
            return liveDataResponse
        }

        liveDataResponse?.elapsedLocalTime = elapsedTime


        return liveDataResponse
    }
}