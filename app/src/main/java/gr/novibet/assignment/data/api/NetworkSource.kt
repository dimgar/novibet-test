package gr.novibet.assignment.data.api

import gr.novibet.assignment.data.api.model.response.games.GamesResponse
import gr.novibet.assignment.data.api.model.response.headlines.HeadlinesResponse
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET

interface NetworkSource {
    @GET("5d7113513300000b2177973a")
    fun games(): Single<List<GamesResponse>>

    @GET("5d7113ef3300000e00779746")
    fun headlines(): Single<List<HeadlinesResponse>>

    @GET("5d7114b2330000112177974d")
    fun updatedGames(): Observable<List<GamesResponse>>

    @GET("5d711461330000d135779748")
    fun updatedHeadlines(): Observable<List<HeadlinesResponse>>
}